import { Body, Controller, NotFoundException, Post } from '@nestjs/common';
import { FollowerDTO } from 'src/domain/dto/follower.dto';
import { FollowerService } from '../services/follower.service';


@Controller('follower')
export class FollowerController {
    constructor(
        private followerService : FollowerService
    ){}

    @Post('/') 
    async newFollower(@Body() followerDTO:FollowerDTO):Promise <any> {

        const service = await this.followerService.register(followerDTO);

        if(!service) throw new NotFoundException('data null');

        return service;

    } 
}
