import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Follower } from 'src/domain/entities/follower.entity';
import { Repository } from 'typeorm';
import { FollowerDTO } from '../../domain/dto/follower.dto'

@Injectable()
export class FollowerService {
    constructor(
        @InjectRepository(Follower) private followerRepo: Repository<Follower>
    ){}

    async register(followerDTO :FollowerDTO):Promise<any> {

        if(!followerDTO) throw new NotFoundException('data null');

        const data = await this.followerRepo.create(followerDTO);

        const result = await this.followerRepo.save(data);

        return result;

    }
}
