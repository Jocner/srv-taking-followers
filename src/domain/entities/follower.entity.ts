import {Entity, ObjectID, ObjectIdColumn, Column} from "typeorm"; 

@Entity() 
export class Follower {  

   @ObjectIdColumn() 
   id: ObjectID;
   
   @Column()
   count_follower: string;
   
   @Column() 
   type_envent: string;
   
   @Column() 
   campus: string;

   @Column() 
   hour: string; 

   
   @Column() 
   date: string; 

   @Column()
   fecha_creado: Date;
   
   
}